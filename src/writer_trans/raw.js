//@flow strict

import {Functor, map} from '../ps/Functor'
import {Apply, apply} from '../ps/Apply'
import {Applicative, pure} from '../ps/Applicative'
import {Tuple, snd} from '../ps/Tuple'
import {unit} from '../ps/Unit'
import {Alt, alt, altLeft} from '../ps/Alt'
import {Newtype, wrap, unwrap} from '../ps/Newtype'
import {Plus, empty, plustLeft} from '../ps/Plus'
import {Bind, bind} from '../ps/Bind'
import {Alternative, alternativeLeft, alternativeRight} from '../ps/Alternative'
import {Monad, monadRight, monadLeft, monadPure, monadBind} from '../ps/Monad'
import {MonadCont, callCC} from '../ps/MonadCont'
import {MonadEff, liftEff} from '../ps/MonadEff'
import {MonadThrow, MonadError, throwError, catchError} from '../ps/MonadError'
import {MonadReader, MonadAsk, ask, local} from '../ps/MonadReader'
import {MonadRec, tailRecM, Loop, Done} from '../ps/MonadRec'
import {MonadState, state} from '../ps/MonadState'
import {MonadTrans, lift} from '../ps/MonadTrans'
import {MonadWriter, MonadTell, tell, listen, pass} from '../ps/MonadWriter'
import {MonadZero, monadZeroLeft, monadZeroRight} from '../ps/MonadZero'
import {MonadPlus, monadPlusValue} from '../ps/MonadPlus'
import { Semigroup, append } from '../ps/Control/Semigroup'
import {Monoid, mempty} from '../ps/Data/Monoid'


function WriterT(x) {
  return x
}
function runWriterT(v) {
  return v
}
const newtypeWriterT = new Newtype(n => n, WriterT)
function monadTransWriterT<A>(dictMonoid: Monoid<A>) {
  return new MonadTrans((dictMonad: Monad<*, *>) => m =>
    monadBind(dictMonad)(m)(v =>
      monadPure(dictMonad, new Tuple(v, mempty(dictMonoid))),
    ),
  )
}
function mapWriterT(f) {
  return (v) => f(v)
}
function functorWriterT(dictFunctor: Functor<*>) {
  return new Functor(f =>
    mapWriterT(map(dictFunctor)(v => new Tuple(f(v.value0), v.value1))),
  )
}
function execWriterT(dictFunctor: Functor<*>) {
  return (v) => map(dictFunctor)(snd)(v)
}
function applyWriterT(dictSemigroup: Semigroup<*>) {
  return function(dictApply: Apply<*, *>) {
    return new Apply(
      () => functorWriterT(dictApply.Functor0()),
      v =>
        function(v1) {
          function k(v3) {
            return function(v4) {
              return new Tuple(
                v3.value0(v4.value0),
                append(dictSemigroup)(v3.value1)(v4.value1),
              )
            }
          }
          return apply(dictApply)(map(dictApply.Functor0())(k)(v))(v1)
        },
    )
  }
}
function bindWriterT(dictSemigroup: Semigroup<*>) {
  return (dictBind: Bind<*, *>) => new Bind(
    () => applyWriterT(dictSemigroup)(dictBind.Apply0()),
    v => (k) => WriterT(
      bind(dictBind)(v)(v1 => {
        const v2 = k(v1.value0)
        return map(dictBind.Apply0().Functor0())(
          v3 => new Tuple(
            v3.value0,
            append(dictSemigroup)(v1.value1)(v3.value1),
          ),
        )(v2)
      }),
    )
  )
}
function applicativeWriterT(dictMonoid) {
  return function(dictApplicative) {
    return new Applicative(
      () => applyWriterT(dictMonoid.Semigroup0())(dictApplicative.Apply0()),
      a =>
        WriterT(
          pure(dictApplicative)(new Tuple(a, mempty(dictMonoid))),
        ),
    )
  }
}
function monadWriterT(dictMonoid) {
  return function(dictMonad: Monad<*, *>) {
    return new Monad(
      () => applicativeWriterT(dictMonoid)(monadLeft(dictMonad)),
      () => bindWriterT(dictMonoid.Semigroup0())(dictMonad.Bind1()),
    )
  }
}
function monadAskWriterT(dictMonoid) {
  return function(dictMonadAsk) {
    return new MonadAsk(
      () => monadWriterT(dictMonoid)(dictMonadAsk.Monad0()),
      lift(monadTransWriterT(dictMonoid))(dictMonadAsk.Monad0())(
        ask(dictMonadAsk),
      ),
    )
  }
}
function monadReaderWriterT(dictMonoid) {
  return function(dictMonadReader) {
    return new MonadReader(
      () => monadAskWriterT(dictMonoid)(dictMonadReader.MonadAsk0()),
      f => mapWriterT(local(dictMonadReader)(f)),
    )
  }
}
function monadContWriterT(dictMonoid) {
  return function(dictMonadCont) {
    return new MonadCont(
      () => monadWriterT(dictMonoid)(dictMonadCont.Monad0()),
      f =>
        WriterT(
          callCC(dictMonadCont)(c => {
            const v = f(a =>
              WriterT(c(new Tuple(a, mempty(dictMonoid)))),
            )
            return v
          }),
        ),
    )
  }
}
function monadEffWriter(dictMonoid) {
  return function(dictMonadEff) {
    return new MonadEff(
      () => monadWriterT(dictMonoid)(dictMonadEff.Monad0()),
      $123 =>
        lift(monadTransWriterT(dictMonoid))(dictMonadEff.Monad0())(
          liftEff(dictMonadEff)($123),
        ),
    )
  }
}
function monadRecWriterT(dictMonoid) {
  return function(dictMonadRec) {
    return new MonadRec(
      () => monadWriterT(dictMonoid)(dictMonadRec.Monad0()),
      f =>
        function(a) {
          function f$prime(v) {
            return bind(monadRight(dictMonadRec.Monad0()))(
              (function() {
                const v1 = f(v.value0)
                return v1
              })(),
            )(v1 =>
              pure(dictMonadRec.Monad0().Applicative0())(
                (function() {
                  if (v1.value0 instanceof Loop) {
                    return new Loop(
                      new Tuple(
                        v1.value0.value0,
                        append(dictMonoid.Semigroup0())(
                          v.value1,
                        )(v1.value1),
                      ),
                    )
                  }
                  if (v1.value0 instanceof Done) {
                    return new Done(
                      new Tuple(
                        v1.value0.value0,
                        append(dictMonoid.Semigroup0())(
                          v.value1,
                        )(v1.value1),
                      ),
                    )
                  }
                  throw new Error(
                    `Failed pattern match at Control.Monad.Writer.Trans line 85, column 16 - line 87, column 47: ${
                      String(
                        [v1.value0.constructor.name],
                      )
                    }`,
                  )
                })(),
              ),
            )
          }
          return WriterT(
            tailRecM(dictMonadRec)(f$prime)(
              new Tuple(a, mempty(dictMonoid)),
            ),
          )
        },
    )
  }
}
function monadStateWriterT(dictMonoid) {
  return function(dictMonadState) {
    return new MonadState(
      () => monadWriterT(dictMonoid)(dictMonadState.Monad0()),
      f =>
        lift(monadTransWriterT(dictMonoid))(dictMonadState.Monad0())(
          state(dictMonadState)(f),
        ),
    )
  }
}
function monadTellWriterT(dictMonoid) {
  return function(dictMonad: Monad<*, *>) {
    return new MonadTell(
      () => monadWriterT(dictMonoid)(dictMonad),
      $124 => WriterT(monadPure(dictMonad, new Tuple(unit, $124))),
    )
  }
}
function monadWriterWriterT(dictMonoid) {
  return function(dictMonad: Monad<*, *>) {
    return new MonadWriter(
      () => monadTellWriterT(dictMonoid)(dictMonad),
      v =>
        monadBind(dictMonad)(v)(v1 =>
          monadPure(
            dictMonad,
            new Tuple(new Tuple(v1.value0, v1.value1), v1.value1),
          ),
        ),
      v =>
        monadBind(dictMonad)(v)(v1 =>
          monadPure(
            dictMonad,
            new Tuple(v1.value0.value0, v1.value0.value1(v1.value1)),
          ),
        ),
    )
  }
}
function monadThrowWriterT(dictMonoid) {
  return function(dictMonadThrow) {
    return new MonadThrow(
      () => monadWriterT(dictMonoid)(dictMonadThrow.Monad0()),
      e =>
        lift(monadTransWriterT(dictMonoid))(dictMonadThrow.Monad0())(
          throwError(dictMonadThrow)(e),
        ),
    )
  }
}
function monadErrorWriterT(dictMonoid) {
  return function(dictMonadError) {
    return new MonadError(
      () => monadThrowWriterT(dictMonoid)(dictMonadError.MonadThrow0()),
      v =>
        function(h) {
          return WriterT(
            catchError(dictMonadError)(v)(e => {
              const v1 = h(e)
              return v1
            }),
          )
        },
    )
  }
}
function altWriterT(dictAlt: Alt<*, *>) {
  return new Alt(
    () => functorWriterT(altLeft(dictAlt)),
    v =>
      function(v1) {
        return alt(dictAlt)(v)(v1)
      },
  )
}
function plusWriterT(dictPlus: Plus<*, *>) {
  return new Plus(() => altWriterT(plustLeft(dictPlus)), empty(dictPlus))
}
function alternativeWriterT(dictMonoid) {
  return function(dictAlternative: Alternative<*, *>) {
    return new Alternative(
      () => applicativeWriterT(dictMonoid)(alternativeLeft(dictAlternative)),
      () => plusWriterT(alternativeRight(dictAlternative)),
    )
  }
}
function monadZeroWriterT(dictMonoid) {
  return function(zero: MonadZero<*, *>) {
    return new MonadZero(
      () => alternativeWriterT(dictMonoid)(monadZeroLeft(zero)),
      () => monadWriterT(dictMonoid)(monadZeroRight(zero)),
    )
  }
}
function monadPlusWriterT(dictMonoid) {
  return function(plus: MonadPlus<*>) {
    return new MonadPlus(() =>
      monadZeroWriterT(dictMonoid)(monadPlusValue(plus)),
    )
  }
}
