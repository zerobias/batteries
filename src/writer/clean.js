//@flow strict
'use strict'

const Data_Functor = (function() {
  function Functor(map) {
    this.map = map
  }
  function map(dict) {
    return dict.map
  }
  return {Functor, map}
})()
const Control_Apply = (function() {
  function Apply(Functor0, apply) {
    this.Functor0 = Functor0
    this.apply = apply
  }
  function apply(dict) {
    return dict.apply
  }
  return {Apply, apply}
})()
const Control_Applicative = (function() {
  function Applicative(Apply0, pure) {
    this.Apply0 = Apply0
    this.pure = pure
  }
  function pure(dict) {
    return dict.pure
  }
  return {
    Applicative,
    pure,
  }
})()
const Data_Newtype = (function() {
  function Newtype(unwrap, wrap) {
    this.unwrap = unwrap
    this.wrap = wrap
  }
  function wrap(dict) {
    return dict.wrap
  }
  function unwrap(dict) {
    return dict.unwrap
  }
  return {
    unwrap,
    wrap,
    Newtype,
  }
})()
const Data_Tuple = (function() {

  function snd(v) {
    return v.value1
  }
  return {snd}
})()
const Control_Monad_Writer_Trans = (function() {
  function WriterT(x) {
    return x
  }
  function runWriterT(v) {
    return v
  }
  function mapWriterT(f) {
    return function(v) {
      return f(v)
    }
  }
  return {WriterT, runWriterT, mapWriterT}
})()
const Data_Identity = (function() {


  function Identity(x) {
    return x
  }
  const newtypeIdentity = new Data_Newtype.Newtype(n => n, Identity)
  const functorIdentity = new Data_Functor.Functor(
    f =>
      function(v) {
        return f(v)
      },
  )
  const applyIdentity = new Control_Apply.Apply(
    () => functorIdentity,
    v =>
      function(v1) {
        return v(v1)
      },
  )
  const applicativeIdentity = new Control_Applicative.Applicative(
    () => applyIdentity,
    Identity,
  )
  return {
    Identity,
    newtypeIdentity,
    functorIdentity,
    applyIdentity,
    applicativeIdentity,
  }
})()
const Control_Monad_Writer = (function() {

  function writer($0) {
    return Control_Monad_Writer_Trans.WriterT(
      Control_Applicative.pure(Data_Identity.applicativeIdentity)($0),
    )
  }
  function runWriter($1) {
    return Data_Newtype.unwrap(Data_Identity.newtypeIdentity)(
      Control_Monad_Writer_Trans.runWriterT($1),
    )
  }
  function mapWriter(f) {
    return Control_Monad_Writer_Trans.mapWriterT($2 =>
      Data_Identity.Identity(
        f(Data_Newtype.unwrap(Data_Identity.newtypeIdentity)($2)),
      ),
    )
  }
  function execWriter(m) {
    return Data_Tuple.snd(runWriter(m))
  }
  return {
    writer,
    runWriter,
    execWriter,
    mapWriter,
  }
})()
