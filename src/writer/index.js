//@flow strict
'use strict'

const PS = {}
const Data_Functor = (function() {
  const Control_Semigroupoid = PS.Control_Semigroupoid
  const Data_Function = PS.Data_Function
  const Data_Unit = PS.Data_Unit
  function Functor(map) {
    this.map = map
  }
  function map(dict) {
    return dict.map
  }
  return {Functor, map}
})()
const Control_Apply = (function() {
  const Control_Category = PS.Control_Category
  const Data_Function = PS.Data_Function
  function Apply(Functor0, apply) {
    this.Functor0 = Functor0
    this.apply = apply
  }
  function apply(dict) {
    return dict.apply
  }
  return {Apply, apply}
})()
const Control_Applicative = (function() {
  const Data_Unit = PS.Data_Unit
  function Applicative(Apply0, pure) {
    this.Apply0 = Apply0
    this.pure = pure
  }
  function pure(dict) {
    return dict.pure
  }
  return {
    Applicative,
    pure,
  }
})()
const Data_Newtype = (function() {
  const Control_Semigroupoid = PS.Control_Semigroupoid
  const Data_Function = PS.Data_Function
  const Prelude = PS.Prelude
  function Newtype(unwrap, wrap) {
    this.unwrap = unwrap
    this.wrap = wrap
  }
  function wrap(dict) {
    return dict.wrap
  }
  function unwrap(dict) {
    return dict.unwrap
  }
  return {
    unwrap,
    wrap,
    Newtype,
  }
})()
const Data_Tuple = (function() {
  const Control_Biapplicative = PS.Control_Biapplicative
  const Control_Biapply = PS.Control_Biapply
  const Control_Bind = PS.Control_Bind
  const Control_Comonad = PS.Control_Comonad
  const Control_Extend = PS.Control_Extend
  const Control_Lazy = PS.Control_Lazy
  const Control_Monad = PS.Control_Monad
  const Control_Semigroupoid = PS.Control_Semigroupoid
  const Data_Bifoldable = PS.Data_Bifoldable
  const Data_Bifunctor = PS.Data_Bifunctor
  const Data_Bitraversable = PS.Data_Bitraversable
  const Data_BooleanAlgebra = PS.Data_BooleanAlgebra
  const Data_Bounded = PS.Data_Bounded
  const Data_CommutativeRing = PS.Data_CommutativeRing
  const Data_Distributive = PS.Data_Distributive
  const Data_Eq = PS.Data_Eq
  const Data_Foldable = PS.Data_Foldable
  const Data_Function = PS.Data_Function
  const Data_Functor_Invariant = PS.Data_Functor_Invariant
  const Data_HeytingAlgebra = PS.Data_HeytingAlgebra
  const Data_Maybe = PS.Data_Maybe
  const Data_Maybe_First = PS.Data_Maybe_First
  const Data_Monoid = PS.Data_Monoid

  const Data_Ord = PS.Data_Ord
  const Data_Ordering = PS.Data_Ordering
  const Data_Ring = PS.Data_Ring
  const Data_Semigroup = PS.Data_Semigroup
  const Data_Semiring = PS.Data_Semiring
  const Data_Show = PS.Data_Show
  const Data_Traversable = PS.Data_Traversable
  const Data_Unit = PS.Data_Unit
  const Prelude = PS.Prelude
  const Type_Equality = PS.Type_Equality
  function snd(v) {
    return v.value1
  }
  return {snd}
})()
const Control_Monad_Writer_Trans = (function() {
  const Control_Alt = PS.Control_Alt
  const Control_Alternative = PS.Control_Alternative

  const Control_Bind = PS.Control_Bind
  const Control_Monad = PS.Control_Monad
  const Control_Monad_Cont_Class = PS.Control_Monad_Cont_Class
  const Control_Monad_Eff_Class = PS.Control_Monad_Eff_Class
  const Control_Monad_Error_Class = PS.Control_Monad_Error_Class
  const Control_Monad_Reader_Class = PS.Control_Monad_Reader_Class
  const Control_Monad_Rec_Class = PS.Control_Monad_Rec_Class
  const Control_Monad_State_Class = PS.Control_Monad_State_Class
  const Control_Monad_Trans_Class = PS.Control_Monad_Trans_Class
  const Control_Monad_Writer_Class = PS.Control_Monad_Writer_Class
  const Control_MonadPlus = PS.Control_MonadPlus
  const Control_MonadZero = PS.Control_MonadZero
  const Control_Plus = PS.Control_Plus
  const Control_Semigroupoid = PS.Control_Semigroupoid
  const Data_Function = PS.Data_Function
  const Data_Monoid = PS.Data_Monoid

  const Data_Semigroup = PS.Data_Semigroup

  const Data_Unit = PS.Data_Unit
  const Prelude = PS.Prelude
  function WriterT(x) {
    return x
  }
  function runWriterT(v) {
    return v
  }
  function mapWriterT(f) {
    return function(v) {
      return f(v)
    }
  }
  return {WriterT, runWriterT, mapWriterT}
})()
const Data_Identity = (function() {
  const Control_Alt = PS.Control_Alt

  const Control_Bind = PS.Control_Bind
  const Control_Comonad = PS.Control_Comonad
  const Control_Extend = PS.Control_Extend
  const Control_Lazy = PS.Control_Lazy
  const Control_Monad = PS.Control_Monad
  const Data_BooleanAlgebra = PS.Data_BooleanAlgebra
  const Data_Bounded = PS.Data_Bounded
  const Data_CommutativeRing = PS.Data_CommutativeRing
  const Data_Eq = PS.Data_Eq
  const Data_EuclideanRing = PS.Data_EuclideanRing
  const Data_Field = PS.Data_Field
  const Data_Foldable = PS.Data_Foldable
  const Data_Functor_Invariant = PS.Data_Functor_Invariant
  const Data_HeytingAlgebra = PS.Data_HeytingAlgebra
  const Data_Monoid = PS.Data_Monoid

  const Data_Ord = PS.Data_Ord
  const Data_Ring = PS.Data_Ring
  const Data_Semigroup = PS.Data_Semigroup
  const Data_Semiring = PS.Data_Semiring
  const Data_Show = PS.Data_Show
  const Data_Traversable = PS.Data_Traversable
  const Prelude = PS.Prelude
  function Identity(x) {
    return x
  }
  const newtypeIdentity = new Data_Newtype.Newtype(n => n, Identity)
  const functorIdentity = new Data_Functor.Functor(
    f =>
      function(v) {
        return f(v)
      },
  )
  const applyIdentity = new Control_Apply.Apply(
    () => functorIdentity,
    v =>
      function(v1) {
        return v(v1)
      },
  )
  const applicativeIdentity = new Control_Applicative.Applicative(
    () => applyIdentity,
    Identity,
  )
  return {
    Identity,
    newtypeIdentity,
    functorIdentity,
    applyIdentity,
    applicativeIdentity,
  }
})()
const Control_Monad_Writer = (function() {
  const Control_Monad_Writer_Class = PS.Control_Monad_Writer_Class
  const Control_Semigroupoid = PS.Control_Semigroupoid

  const Prelude = PS.Prelude
  function writer($0) {
    return Control_Monad_Writer_Trans.WriterT(
      Control_Applicative.pure(Data_Identity.applicativeIdentity)($0),
    )
  }
  function runWriter($1) {
    return Data_Newtype.unwrap(Data_Identity.newtypeIdentity)(
      Control_Monad_Writer_Trans.runWriterT($1),
    )
  }
  function mapWriter(f) {
    return Control_Monad_Writer_Trans.mapWriterT($2 =>
      Data_Identity.Identity(
        f(Data_Newtype.unwrap(Data_Identity.newtypeIdentity)($2)),
      ),
    )
  }
  function execWriter(m) {
    return Data_Tuple.snd(runWriter(m))
  }
  return {
    writer,
    runWriter,
    execWriter,
    mapWriter,
  }
})()
