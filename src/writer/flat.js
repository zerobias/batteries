//@flow strict
'use strict'

import {Functor, map} from '../ps/Functor'
import {Apply, apply} from '../ps/Apply'
import {Applicative, pure} from '../ps/Applicative'
import {Tuple, snd} from '../ps/Tuple'
import {Newtype, unwrap} from '../ps/Newtype'

function WriterT(x) {
  return x
}
function runWriterT(v) {
  return v
}
function mapWriterT(f) {
  return (v: *) => f(v)
}

function Identity<I>(x: I) {
  return x
}
const newtypeIdentity = new Newtype(Identity, Identity)
const functorIdentity = new Functor(
  f => v => f(v),
)
const applyIdentity = new Apply(
  () => functorIdentity,
  v => v1 => v(v1),
)
const applicativeIdentity = new Applicative(
  () => applyIdentity,
  Identity,
)

export function writer($0) {
  return WriterT(
    pure(applicativeIdentity)($0),
  )
}

const Control_Monad_Writer = (function() {

  function runWriter($1) {
    return unwrap(newtypeIdentity)(
      runWriterT($1),
    )
  }
  function mapWriter(f) {
    return mapWriterT($2 =>
      Identity(
        f(unwrap(newtypeIdentity)($2)),
      ),
    )
  }
  function execWriter(m) {
    return snd(runWriter(m))
  }
  return {
    runWriter,
    execWriter,
    mapWriter,
  }
})()
