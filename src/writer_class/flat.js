//@flow strict
'use strict'

import {Functor, map} from '../ps/Functor'
import {Apply, apply} from '../ps/Apply'
import {Applicative, pure} from '../ps/Applicative'
import {Tuple, snd} from '../ps/Tuple'
import {bind} from '../ps/Bind'

export class MonadTell<V0, V1> {
  /*::+*/Monad0: V0
  /*::+*/tell: V1
  constructor(Monad0: V0, tell: V1) {
    this.Monad0 = Monad0
    this.tell = tell
  }
}

export class MonadWriter<V0, V1, V2> {
  /*::+*/MonadTell0: V0
  /*::+*/listen: V1
  /*::+*/pass: V2
  constructor(MonadTell0: V0, listen: V1, pass: V2) {
    this.MonadTell0 = MonadTell0
    this.listen = listen
    this.pass = pass
  }
}

function tell({tell}) {
  return tell
}
function pass({pass}) {
  return pass
}
function listen({listen}) {
  return listen
}
function listens(dictMonadWriter) {
  return function(f) {
    return function(m) {
      return bind(
        dictMonadWriter
          .MonadTell0()
          .Monad0()
          .Bind1(),
      )(listen(dictMonadWriter)(m))(function inner<V0, V1>({value0, value1}: Tuple<V0, V1>) {
        return pure(
          dictMonadWriter
            .MonadTell0()
            .Monad0()
            .Applicative0(),
        )(new Tuple(value0, f(value1)))
      })
    }
  }
}
function censor(dictMonadWriter) {
  return function(f) {
    return function(m) {
      return pass(dictMonadWriter)(
        bind(
          dictMonadWriter
            .MonadTell0()
            .Monad0()
            .Bind1(),
        )(m)((v) => pure(
          dictMonadWriter
            .MonadTell0()
            .Monad0()
            .Applicative0(),
        )(new Tuple(v, f))),
      )
    }
  }
}

// Control_Monad_Writer_Class
