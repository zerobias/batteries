//@flow strict

import {Show} from '../Show'

export function Void<T>(x: T) {
  return x
}
export function absurd<T>(a: T): empty {
  function spin($copy): empty {
    let $tco_result
    let $copy_v = $copy
    function $tco_loop(v) {
      $copy_v = v
      return
    }
    while (!false) {
      $tco_result = $tco_loop($copy_v)
    }
    throw new Error('absurd')
    // return $tco_result
  }
  return spin(a)
}
export const showVoid: Show<*> = new Show(absurd)
