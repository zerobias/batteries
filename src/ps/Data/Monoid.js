//@flow strict

import { otherwise } from './Boolean'
// const Data_Eq = require('../Data.Eq')
// const Data_EuclideanRing = require('../Data.EuclideanRing')
import {$$const} from './Function'
//const Data_Ord = require('../Data.Ord')
//const Data_Ordering = require('../Data.Ordering')
import {
  semigroupUnit, semigroupString, semigroupArray, semigroupFn, append,
  type Semigroup,
} from '../Control/Semigroup'
import {unit, type Unit} from '../Unit'
//const Prelude = require('../Prelude')
export class Monoid<S> {
  /*::+*/Semigroup0: () => Semigroup<S>
  /*::+*/mempty: S
  constructor(Semigroup0: () => Semigroup<S>, mempty: S): Monoid<S> {
    this.Semigroup0 = Semigroup0
    this.mempty = mempty
    return this
  }
}

export const monoidUnit: Monoid<Unit> = new Monoid(() => semigroupUnit, unit)
export const monoidString: Monoid<string> = new Monoid(() => semigroupString, '')
//const monoidOrdering = new Monoid((() => Data_Ordering.semigroupOrdering), Data_Ordering.EQ.value)
const monoidArray = new Monoid((() => semigroupArray), [  ])
export function mempty<S>({mempty}: Monoid<S>): S {
  return mempty
}
function monoidFn(dictMonoid) {
  return new Monoid((() => semigroupFn(dictMonoid.Semigroup0())), $$const(mempty(dictMonoid)))
}
function power(dictMonoid) {
  return function(x) {
    function go(p) {
      if (p <= 0) {
        return mempty(dictMonoid)
      }
      if (p === 1) {
        return x
      }
      if (p % 2 === 0) {
        const x$prime = go(p / 2 | 0)
        return append(dictMonoid.Semigroup0())(x$prime)(x$prime)
      }
      if (otherwise) {
        const x$prime = go(p / 2 | 0)
        return append(dictMonoid.Semigroup0())(x$prime)(append(dictMonoid.Semigroup0())(x$prime)(x))
      }
      throw new Error(`Failed pattern match at Data.Monoid line 52, column 3 - line 52, column 17: ${
        String([p.constructor.name])
      }`)
    }
    return go
  }
}
function guard(dictMonoid) {
  return function(v) {
    return function(v1) {
      if (v) {
        return v1
      }
      if (!v) {
        return mempty(dictMonoid)
      }
      throw new Error(`Failed pattern match at Data.Monoid line 60, column 1 - line 60, column 49: ${
        String([v.constructor.name, v1.constructor.name])
      }`)
    }
  }
}
