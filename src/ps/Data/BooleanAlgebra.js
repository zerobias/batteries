//@flow strict

import {
  heytingAlgebraBoolean,
  heytingAlgebraUnit,
  heytingAlgebraFunction,
  type HeytingAlgebra,
} from './HeytingAlgebra'

import type {Unit} from '../Unit'

export class BooleanAlgebra<T> {
  /*::+*/HeytingAlgebra0: () => HeytingAlgebra<T>
  constructor(HeytingAlgebra0: () => HeytingAlgebra<T>) {
    this.HeytingAlgebra0 = HeytingAlgebra0
  }
}

export function booleanAlgebraValue<T>(
  {HeytingAlgebra0}: BooleanAlgebra<T>
): HeytingAlgebra<T> {
  return HeytingAlgebra0()
}


export const booleanAlgebraUnit: BooleanAlgebra<Unit> = new BooleanAlgebra(() => heytingAlgebraUnit)

export function booleanAlgebraFn<A, B>(
  dictBooleanAlgebra: BooleanAlgebra<B>
): BooleanAlgebra<(a: A) => B> {
  return new BooleanAlgebra(
    (): HeytingAlgebra<(a: A) => B> => heytingAlgebraFunction(booleanAlgebraValue(dictBooleanAlgebra))
  )
}
export const booleanAlgebraBoolean: BooleanAlgebra<boolean> = new BooleanAlgebra(() => heytingAlgebraBoolean)
