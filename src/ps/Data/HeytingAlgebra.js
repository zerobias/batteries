//@flow strict

import {unit, type Unit} from '../Unit'

export class HeytingAlgebra<T> {
  /*::+*/conj: (a: T) => (b: T) => T
  /*::+*/disj: (a: T) => (b: T) => T
  /*::+*/ff: T
  /*::+*/implies: (a: T) => (b: T) => T
  /*::+*/not: (b: T) => T
  /*::+*/tt: T
  constructor(
    conj: (a: T) => (b: T) => T,
    disj: (a: T) => (b: T) => T,
    ff: T,
    implies: (a: T) => (b: T) => T,
    not: (b: T) => T,
    tt: T
  ) {
    this.conj = conj
    this.disj = disj
    this.ff = ff
    this.implies = implies
    this.not = not
    this.tt = tt
  }
}

export function tt<T>({tt}: HeytingAlgebra<T>): T {
  return tt
}
export function not<T>({not}: HeytingAlgebra<T>): ((b: T) => T) {
  return not
}
export function implies<T>({implies}: HeytingAlgebra<T>): ((a: T) => (b: T) => T) {
  return implies
}
export function ff<T>({ff}: HeytingAlgebra<T>): T {
  return ff
}
export function disj<T>({disj}: HeytingAlgebra<T>): ((a: T) => (b: T) => T) {
  return disj
}
export function conj<T>({conj}: HeytingAlgebra<T>): ((a: T) => (b: T) => T) {
  return conj
}

export const heytingAlgebraUnit: HeytingAlgebra<Unit> = new HeytingAlgebra(
  v => v1 => unit,
  v => v1 => unit,
  unit,
  v => v1 => unit,
  v => unit,
  unit,
)

export const heytingAlgebraBoolean: HeytingAlgebra<boolean> = new HeytingAlgebra(
  boolConj,
  boolDisj,
  false,
  (a: boolean) => (b: boolean): boolean => disj(heytingAlgebraBoolean)(not(heytingAlgebraBoolean)(a))(b),
  boolNot,
  true,
)

export function heytingAlgebraFunction<A, B>(
  dictHeytingAlgebra: HeytingAlgebra<B>
): HeytingAlgebra<(a: A) => B> {
  return new HeytingAlgebra(
    (f: (a: A) => B) => (g: (a: A) => B) => (a: A) => conj(dictHeytingAlgebra)(f(a))(g(a)),
    (f: (a: A) => B) => (g: (a: A) => B) => (a: A) => disj(dictHeytingAlgebra)(f(a))(g(a)),
    v => ff(dictHeytingAlgebra),
    (f: (a: A) => B) => (g: (a: A) => B) => (a: A) => implies(dictHeytingAlgebra)(f(a))(g(a)),
    (f: (a: A) => B) => (a: A) => not(dictHeytingAlgebra)(f(a)),
    v => tt(dictHeytingAlgebra),
  )
}

function boolConj(b1: boolean) {
  return (b2: boolean) =>  b1 && b2
}

function boolDisj(b1: boolean) {
  return (b2: boolean) =>  b1 || b2
}

function boolNot(b: boolean) {
  return !b
}
