//@flow strict

function on(f) {
  return (g) => (x) => (y) => f(g(x))(g(y))
}
function flip(f) {
  return (b) => (a) => f(a)(b)
}
export function $$const<A>(a: A): ((v: *) => A) {
  return (v) => a
}
function applyFlipped(x) {
  return (f) => f(x)
}
function apply(f) {
  return (x) => f(x)
}
