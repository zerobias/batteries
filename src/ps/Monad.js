//@flow strict

import {Bind, bind} from './Bind'
import {Applicative, pure} from './Applicative'

export class Monad<A, B> {
  /*::+*/Applicative0: A
  /*::+*/Bind1: B
  constructor(Applicative0: A, Bind1: B) {
    this.Applicative0 = Applicative0
    this.Bind1 = Bind1
  }
}

export function monadBind<B0, B1>(
  {Bind1}: Monad<*, () => Bind<*, (a: B0) => B1>>
): ((a: B0) => B1) {
  return bind(Bind1())
}

export function monadPure<A0, A1>(
  {Applicative0}: Monad<() => Applicative<*, (a: A0) => A1>, *>,
  data: A0,
): A1 {
  return pure(Applicative0())(data)
}

export function monadLeft<A0, A1>(
  {Applicative0}: Monad<() => Applicative<A0, A1>, *>
): Applicative<A0, A1> {
  return Applicative0()
}

export function monadRight<B0, B1>(
  {Bind1}: Monad<*, () => Bind<B0, B1>>
): Bind<B0, B1> {
  return Bind1()
}
