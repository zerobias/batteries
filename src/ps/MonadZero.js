//@flow strict
import {Alternative} from './Alternative'
import {Monad} from './Monad'

export class MonadZero<A, B> {
  /*::+*/Alternative1: A
  /*::+*/Monad0: B
  constructor(
    Alternative1: A,
    Monad0: B,
  ) {
    this.Alternative1 = Alternative1
    this.Monad0 = Monad0
  }
}

export function monadZeroLeft<A0, A1>(
  {Alternative1}: MonadZero<() => Alternative<A0, A1>, *>
): Alternative<A0, A1> {
  return Alternative1()
}

export function monadZeroRight<B0, B1>(
  {Monad0}: MonadZero<*, () => Monad<B0, B1>>
): Monad<B0, B1> {
  return Monad0()
}
