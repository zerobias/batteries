//@flow strict

import {Functor} from './Functor'

export class Apply<F, A> {
  /*::+*/Functor0: () => Functor<F>
  /*::+*/apply: A
  constructor(Functor0: () => Functor<F>, apply: A) {
    this.Functor0 = Functor0
    this.apply = apply
  }
}

export function apply<A, B>({apply}: Apply<*, (a: A) => B>): ((a: A) => B) {
  return apply
}