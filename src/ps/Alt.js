//@flow strict

import {Functor} from './Functor'

export class Alt<F, A> {
  /*::+*/Functor0: F
  /*::+*/alt: A
  constructor(Functor0: F, alt: A) {
    this.Functor0 = Functor0
    this.alt = alt
  }
}

export function altLeft<F>({Functor0}: Alt<() => Functor<F>, *>): Functor<F> {
  return Functor0()
}

export function alt<A, B>({alt}: Alt<*, (a: A) => B>): ((a: A) => B) {
  return alt
}
