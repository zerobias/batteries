//@flow strict


export class MonadAsk<A, P> {
  /*::+*/Monad0: A
  /*::+*/ask: P
  constructor(Monad0: A, ask: P) {
    this.Monad0 = Monad0
    this.ask = ask
  }
}

export function ask<A, B>({ask}: MonadAsk<*, (a: A) => B>): ((a: A) => B) {
  return ask
}

export function local<A, B>({local}: MonadReader<*, (a: A) => B>): ((a: A) => B) {
  return local
}

export class MonadReader<A, P> {
  /*::+*/MonadAsk0: A
  /*::+*/local: P
  constructor(MonadAsk0: A, local: P) {
    this.MonadAsk0 = MonadAsk0
    this.local = local
  }
}

