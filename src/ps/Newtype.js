//@flow strict

export class Newtype<U, W> {
  /*::+*/unwrap: U
  /*::+*/wrap: W
  constructor(unwrap: U, wrap: W) {
    this.unwrap = unwrap
    this.wrap = wrap
  }
}

export function wrap<U1, U2>(
  {wrap}: Newtype<*, (u1: U1) => U2>
): ((u1: U1) => U2) {
  return wrap
}
export function unwrap<U1, U2>(
  {unwrap}: Newtype<(u1: U1) => U2, *>
): ((u1: U1) => U2) {
  return unwrap
}
