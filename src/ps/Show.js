//@flow strict

export class Show<T> {
  /*::+*/show: (t: T) => string
  constructor(show: (t: T) => string) {
    this.show = show
  }
}

export function show<T>({show}: Show<T>): ((t: T) => string) {
  return show
}

export const showString: Show<string> = new Show(showStringImpl)
export const showNumber: Show<number> = new Show(showNumberImpl)
export const showInt: Show<number> = new Show(showIntImpl)
export const showChar: Show<string> = new Show(showCharImpl)
export const showBoolean: Show<boolean> = new Show((v: boolean) => {
  if (v) {
    return 'true'
  }
  if (!v) {
    return 'false'
  }
  throw new Error(`Failed pattern match at Data.Show line 12, column 1 - line 12, column 37: ${
    String([v.constructor.name])
  }`)
})

export function showArray<T>(dictShow: Show<T>): Show<T[]> {
  return new Show(showArrayImpl(show(dictShow)))
}

function showIntImpl(n: number) {
  return n.toString()
}

function showNumberImpl(n: number) {
  const str = n.toString()
  return isNaN(`${str}.0`) ? str : `${str}.0`
}

function showCharImpl(c: string) {
  const code = c.charCodeAt(0)
  if (code < 0x20 || code === 0x7F) {
    switch (c) {
      case '\x07': return "'\\a'"
      case '\b': return "'\\b'"
      case '\f': return "'\\f'"
      case '\n': return "'\\n'"
      case '\r': return "'\\r'"
      case '\t': return "'\\t'"
      case '\v': return "'\\v'"
    }
    return `'\\${code.toString(10)}'`
  }
  return c === "'" || c === '\\' ? `'\\${c}'` : `'${c}'`
}

function showStringImpl(s: string) {
  return `"${s.replace(
    /[\0-\x1F\x7F"\\]/g,
    (c, i) => {
      switch (c) {
        case '"':
        case '\\':
          return `\\${c}`
        case '\x07': return '\\a'
        case '\b': return '\\b'
        case '\f': return '\\f'
        case '\n': return '\\n'
        case '\r': return '\\r'
        case '\t': return '\\t'
        case '\v': return '\\v'
      }
      const k = i + 1
      const empty = k < s.length && s[k] >= '0' && s[k] <= '9' ? '\\&' : ''
      return `\\${c.charCodeAt(0).toString(10)}${empty}`
    }
  )}"`
}

function showArrayImpl<T>(f: (x: T) => string): ((l: T[]) => string) {
  return function(xs: $ReadOnlyArray<T>) {
    const ss = []
    for (let i = 0, l = xs.length; i < l; i++) {
      ss[i] = f(xs[i])
    }
    return `[${ss.join(',')}]`
  }
}


