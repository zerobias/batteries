//@flow strict


export class MonadTell<A, P> {
  /*::+*/Monad0: A
  /*::+*/tell: P
  constructor(Monad0: A, tell: P) {
    this.Monad0 = Monad0
    this.tell = tell
  }
}

export class MonadWriter<M, L, P> {
  /*::+*/MonadTell0: M
  /*::+*/listen: L
  /*::+*/pass: P
  constructor(MonadTell0: M, listen: L, pass: P) {
    this.MonadTell0 = MonadTell0
    this.listen = listen
    this.pass = pass
  }
}

export function tell<A, B>({tell}: MonadTell<*, (a: A) => B>): ((a: A) => B) {
  return tell
}

export function listen<A, B>({listen}: MonadWriter<*, (a: A) => B, *>): ((a: A) => B) {
  return listen
}

export function pass<A, B>({pass}: MonadWriter<*, *, (a: A) => B>): ((a: A) => B) {
  return pass
}
