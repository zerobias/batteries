//@flow strict

export class Tuple<V0, V1> {
  /*::+*/value0: V0
  /*::+*/value1: V1
  constructor(value0: V0, value1: V1) {
    this.value0 = value0
    this.value1 = value1
  }
}

export function create<T0>(value0: T0) {
  return function<T1>(value1: T1): Tuple<T0, T1> {
    return new Tuple(value0, value1)
  }
}

export function snd<A, B>({value1}: Tuple<*, (a: A) => B>): ((a: A) => B) {
  return value1
}
