//@flow strict

export class MonadCont<A, P> {
  /*::+*/Monad0: A
  /*::+*/callCC: P
  constructor(Monad0: A, callCC: P) {
    this.Monad0 = Monad0
    this.callCC = callCC
  }
}

export function callCC<A, B>({callCC}: MonadCont<*, (a: A) => B>): ((a: A) => B) {
  return callCC
}
