//@flow strict

export class MonadEff<A, P> {
  /*::+*/Monad0: A
  /*::+*/liftEff: P
  constructor(Monad0: A, liftEff: P) {
    this.Monad0 = Monad0
    this.liftEff = liftEff
  }
}

export function liftEff<A, B>({liftEff}: MonadEff<*, (a: A) => B>): ((a: A) => B) {
  return liftEff
}
