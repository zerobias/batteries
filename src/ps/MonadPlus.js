//@flow strict

import {MonadZero} from './MonadZero'

export class MonadPlus<T> {
  /*::+*/MonadZero0: T
  constructor(MonadZero0: T) {
    this.MonadZero0 = MonadZero0
  }
}

export function monadPlusValue<A, B>(
  {MonadZero0}: MonadPlus<() => MonadZero<A, B>>
): MonadZero<A, B> {
  return MonadZero0()
}
