//@flow strict


export class MonadTrans<D> {
  /*::+*/lift: D
  constructor(lift: D) {
    this.lift = lift
  }
}

export function lift<A, B>({lift}: MonadTrans<(a: A) => B>): ((a: A) => B) {
  return lift
}