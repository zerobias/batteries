//@flow strict

export class MonadThrow<A, P> {
  /*::+*/Monad0: A
  /*::+*/throwError: P
  constructor(Monad0: A, throwError: P) {
    this.Monad0 = Monad0
    this.throwError = throwError
  }
}

export function throwError<A, B>({throwError}: MonadThrow<*, (a: A) => B>): ((a: A) => B) {
  return throwError
}

export class MonadError<A, P> {
  /*::+*/MonadThrow0: A
  /*::+*/catchError: P
  constructor(MonadThrow0: A, catchError: P) {
    this.MonadThrow0 = MonadThrow0
    this.catchError = catchError
  }
}

export function catchError<A, B>({catchError}: MonadError<*, (a: A) => B>): ((a: A) => B) {
  return catchError
}
