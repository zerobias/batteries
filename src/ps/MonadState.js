//@flow strict


export class MonadState<A, P> {
  /*::+*/Monad0: A
  /*::+*/state: P
  constructor(Monad0: A, state: P) {
    this.Monad0 = Monad0
    this.state = state
  }
}

export function state<A, B>({state}: MonadState<*, (a: A) => B>): ((a: A) => B) {
  return state
}
