//@flow strict

import {semigroupoidFn, type Semigroupoid} from './Semigroupoid'

export class Category<S, I> {
  /*::+*/Semigroupoid0: () => Semigroupoid<S>
  /*::+*/id: I
  constructor(Semigroupoid0: () => Semigroupoid<S>, id: I) {
    this.Semigroupoid0 = Semigroupoid0
    this.id = id
  }
}


export function id<I>({id}: Category<*, I>): I {
  return id
}
export function categoryFn<S>(): Category<S, *> {
  return new Category(semigroupoidFn, <I>(x: I) => x)
}
