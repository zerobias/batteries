//@flow strict

type Id<T> = (x: T) => T
type IdFun<T> = (f: Id<T>) => (g: Id<T>) => Id<T>

export class Semigroupoid<T> {
  /*::+*/compose: IdFun<T>
  constructor(compose: IdFun<T>) {
    this.compose = compose
  }
}

export function semigroupoidFn<T>(): Semigroupoid<T> {
  return new Semigroupoid((f) => (g) => (x) => f(g(x)))
}
export function compose<T>({compose}: Semigroupoid<T>): IdFun<T> {
  return compose
}
export function composeFlipped<T>(dictSemigroupoid: Semigroupoid<T>): IdFun<T> {
  return (f: *) => (g: *) => compose(dictSemigroupoid)(g)(f)
}
