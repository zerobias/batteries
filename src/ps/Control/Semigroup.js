//@flow strict

import {unit, type Unit} from '../Unit'
import {absurd} from '../Data/Void'

type Id<T> = (x: T) => T

export class Semigroup<T> {
  /*::+*/append: (a: T) => (b: T) => T
  constructor(append: (a: T) => (b: T) => T) {
    this.append = append
  }
}

export const semigroupVoid: Semigroup<empty> = new Semigroup((v) => absurd)
export const semigroupUnit: Semigroup<Unit> = new Semigroup((v) => (v1) => unit)
export const semigroupString: Semigroup<string> = new Semigroup(concatString)
export const semigroupArray: Semigroup<$ReadOnlyArray<*>> = new Semigroup(concatArray)
export function append<T>({append}: Semigroup<T>): ((a: T) => (b: T) => T) {
  return append
}
export function semigroupFn<T>(dictSemigroup: Semigroup<T>): Semigroup<Id<T>> {
  return new Semigroup((f: Id<T>) => (g: Id<T>) => (x: T) => append(dictSemigroup)(f(x))(g(x)))
}

function concatString(s1) {
  return function(s2) {
    return s1 + s2
  }
}

function concatArray(xs) {
  return function(ys) {
    if (xs.length === 0) return ys
    if (ys.length === 0) return xs
    return xs.concat(ys)
  }
}
