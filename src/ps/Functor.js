//@flow strict

export class Functor<F> {
  /*::+*/map: F
  constructor(map: F) {
    this.map = map
  }
}

export function map<A, B>({map}: Functor<(a: A) => B>): ((a: A) => B) {
  return map
}
