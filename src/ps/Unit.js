//@flow strict

import {Show} from './Show'

export class Unit {}

export const unit = new Unit()

export const showUnit: Show<Unit> = new Show(u => 'unit')
