//@flow strict
import {Applicative} from './Applicative'
import {Plus} from './Plus'

export class Alternative<A, P> {
  /*::+*/Applicative0: A
  /*::+*/Plus1: P
  constructor(Applicative0: A, Plus1: P) {
    this.Applicative0 = Applicative0
    this.Plus1 = Plus1
  }
}

export function alternativeLeft<A0, A1>(
  {Applicative0}: Alternative<() => Applicative<A0, A1>, *>
): Applicative<A0, A1> {
  return Applicative0()
}

export function alternativeRight<B0, B1>(
  {Plus1}: Alternative<*, () => Plus<B0, B1>>
): Plus<B0, B1> {
  return Plus1()
}
