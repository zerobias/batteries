//@flow strict

import {Alt} from './Alt'

export class Plus<F, A> {
  /*::+*/Alt0: F
  /*::+*/empty: A
  constructor(Alt0: F, empty: A) {
    this.Alt0 = Alt0
    this.empty = empty
  }
}

export function plustLeft<A, B>({Alt0}: Plus<() => Alt<A, B>, *>): Alt<A, B> {
  return Alt0()
}

export function empty<A, B>({empty}: Plus<*, (a: A) => B>): ((a: A) => B) {
  return empty
}
