//@flow strict

export class Applicative<A, P> {
  /*::+*/Apply0: A
  /*::+*/pure: P
  constructor(Apply0: A, pure: P) {
    this.Apply0 = Apply0
    this.pure = pure
  }
}

export function pure<A, B>({pure}: Applicative<*, (a: A) => B>): ((a: A) => B) {
  return pure
}
