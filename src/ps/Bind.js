//@flow strict

export class Bind<F, A> {
  /*::+*/Apply0: F
  /*::+*/bind: A
  constructor(Apply0: F, bind: A) {
    this.Apply0 = Apply0
    this.bind = bind
  }
}

export function bind<A, B>({bind}: Bind<*, (a: A) => B>): ((a: A) => B) {
  return bind
}
