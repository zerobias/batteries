//@flow strict


export class Loop<A> {
  /*::+*/value0: A
  constructor(value0: A) {
    this.value0 = value0
  }
}

export class Done<D> {
  /*::+*/value0: D
  constructor(value0: D) {
    this.value0 = value0
  }
}

export function done<A, B>({value0}: Done<(a: A) => B>): ((a: A) => B) {
  return value0
}

export function loop<A, B>({value0}: Loop<(a: A) => B>): ((a: A) => B) {
  return value0
}

export class MonadRec<A, P> {
  /*::+*/Monad0: A
  /*::+*/tailRecM: P
  constructor(Monad0: A, tailRecM: P) {
    this.Monad0 = Monad0
    this.tailRecM = tailRecM
  }
}

export function tailRecM<A, B>(
  {tailRecM}: MonadRec<*, (a: A) => B>
): ((a: A) => B) {
  return tailRecM
}

